## 目的

これは、CentOS6の素の環境からなるべく短時間でRailsをインストールすることを目的としています。ソースコードは特になく、[WIKI](https://bitbucket.org/ichiking/rocket_start_rails/wiki/Home)を御覧ください。
